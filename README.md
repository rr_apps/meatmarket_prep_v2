# MeatMarket Prep ver 2.0 #

This script supercedes the former AppleScript version.

Features improved parsing and better reading of regions with far less editing. 

## Usage:


* In the `meatmarket_prep_v2/jobs` directory create a dir with todays date: `meatmarket_prep_v2/jobs/2017-09-04`
* Then place the excel you want to process in the dir. `meatmarket_prep_v2/jobs/2017-09-04/Specials for Week 04 - 10 September 2017.xlsx`
* In Terminal change dir to `lib` and run tha script like this;
	
    	ruby reader.rb ../jobs/2017-08-04/Specials\ for\ the\ week\ \ 07\ -13\ August\ 2017.xlsx

* Once it has run there will be a CSV file in the same directory.
