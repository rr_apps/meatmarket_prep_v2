require 'roo'
require 'roo-xls'
require 'pp'
require 'fileutils'
require_relative '../data/regions'

file = ARGV.shift
if file.nil?
  puts %(Usage:  ruby reader.rb <excel file>
          excel file should be of type [.xlsx]
  )
end

OUTPUT_FILE       = file.gsub('.xlsx', '.csv').gsub('.xls', '.csv')
CLIENT_FILE       = Roo::Spreadsheet.open(file)
ITEM_PROD_SELECT  = '../data/itemProdSelect.csv'.freeze
BRAND_SHORTS      = '../data/brands.csv'.freeze

keys = %w[long_brand short_brand]
@brands_list = CSV.parse(File.read(BRAND_SHORTS)).map { |a| Hash[keys.zip(a)] }

keys = %w[item_code item_descr prod_code prod_descr]
@item_prod_list = CSV.parse(File.read(ITEM_PROD_SELECT)).map { |a| Hash[keys.zip(a)] }

def spy(msg, len = 35)
  print '-' * len + "\n" + caller[0].split('/')[-1] + "\n" + msg.class.to_s
  print ' : ' + msg.to_s + "\n" + '-' * len + "\n" * 2
end

def product_from_item_code(item_code)
  @item_prod_list.each do |item|
    return item if item['item_code'].to_s.strip == item_code.to_s.strip
  end
  "Not known in Item to Product list: #{item_code}"
end

def brand_short_from_brand(brand_long)
  @brands_list.each do |brand|
    return brand['short_brand'] if brand['long_brand'].to_s.strip.casecmp(brand_long.downcase).zero?
  end
  'Not known in Brand to Short Brand list'
end

def region_cleaner(_object)
  region.downcase
        .delete(' ')
        .gsub('store', '')
        .gsub('stores', '')
        .gsub('division', '')
        .gsub('divisions', '')
        .gsub('checkers', '')
        .gsub('shoprite', '')
        .gsub('hyper', '')
        .delete('&')
end

def region?(object)
  REGIONS.each do |reg|
    region = reg['region'].to_s.downcase.strip.delete(' ')
    req = object[0].to_s.downcase.strip.delete(' ')
    next if req.nil?
    return reg if req.include?(region)
  end
  false
end

def a_num?(str)
  !!Integer(str)
rescue ArgumentError, TypeError
  false
end

def a_flighting?(row)
  row[0].is_a?(Float) || row[0].is_a?(Integer) || a_num?(row[0].to_s)
end

def language_iterator(languages)
  languages.split('&').map! { |l| l.strip.downcase.capitalize }
end

def lang_code_select(language)
  return 'L01' if language.downcase.include?('afr')
  'L02'
end

def generate_price_code(price)
  res = format('%0.2f', price.downcase.delete('r').to_f)

  res = res.delete('.')
  "PR#{format '%06d', res}"
end

def compile_day_of_week(start_date, end_date)
  days = ''
  (Date.parse(start_date.to_s)..Date.parse(end_date.to_s)).each do |date|
    days << date.wday.to_s
  end
  days.tr('0', '7')
end

def hal_store?(prod_descr)
  return ' EX HAL' if prod_descr.downcase.include?('pork')
  ''
end

def read_rows(row)
  item_code = row[0].to_i
  brand_long = row[2].strip
  brand_short = brand_short_from_brand(brand_long)
  region_short = @region_short
  date_from = row[3]
  date_to = row[4]
  price = row[5].to_s.strip
  unit = row[6].delete('/').strip
  [brand_short, date_from, date_to, item_code, price, region_short, unit]
end

def product_stuff(item_code)
  product_from_item = product_from_item_code(item_code)
  product_descr = product_from_item['prod_descr']
  product_code = product_from_item['prod_code']
  unless product_code.nil?

    product_code = product_code.split('&')[0].strip if product_code.include?('&')
  end
  [product_code, product_descr]
end

def theme_stuff
  theme = 'Standard'
  theme_code = 'T0001'
  [theme, theme_code]
end

def date_stuff(date_from, date_to)
  start_date = date_from.strftime('%d/%m/%Y').to_s
  end_date = date_to.strftime('%d/%m/%Y').to_s
  [end_date, start_date]
end

def lang_full(lang)
  return 'English' if lang.downcase.include?('eng')
  'Afrikaans'
end
header = ['Description', 'Product', 'Product Code', 'Language', 'Language Code', 'Theme', 'Theme Code', 'Price', 'Price code', 'Start Date', 'End Date', 'Weekdays', 'Playing At', 'Store Listing File']
list_of_adverts = []
list_of_adverts << header
playing_at = 9

CLIENT_FILE.each do |row|
  region = region?(row.compact)
  if region

    spy @region_long  = region['region']
    @region_short     = region['reg']
    @region_lang      = region['lang']
  end

  next unless a_flighting?(row)

  language_iterator(@region_lang).each do |language|
    brand_short, date_from, date_to, item_code, price, region_short, unit = read_rows(row)
    product_code, product_descr = product_stuff(item_code)

    next if product_descr.nil?
    product_descr = product_descr.delete(',')
    ex_hal_store = hal_store?(product_descr)
    price = price.downcase.delete('r').gsub('         ', ' ').delete(' ')

    # next if language.downcase.include?('afr') && ex_hal_store.downcase.include?('hal')
    description = "#{product_descr.strip} R#{price} #{unit} #{region_short} "
    description << "#{brand_short + ex_hal_store} #{language}"
    product	= product_descr.strip
    language_code	= lang_code_select(language)
    theme, theme_code = theme_stuff
    price = "R#{format('%0.2f', price.to_f)}"
    end_date, start_date = date_stuff(date_from, date_to)
    price_code = generate_price_code(price)
    weekdays = compile_day_of_week(start_date, end_date)
    store_listing_file = "#{region_short} #{brand_short} #{language}.DSF".upcase

    # Build first timeslot advert
    advert = []
    advert << description << product << product_code << lang_full(language)
    advert << language_code << theme << theme_code
    advert << price << price_code << start_date << end_date
    advert << weekdays << (playing_at += 1).to_s << store_listing_file
    # p advert
    list_of_adverts << advert

    # Build second timeslot advert
    advert = []
    advert << description + '2' << product << product_code << lang_full(language)
    advert << language_code << theme << theme_code
    advert << price << price_code << start_date << end_date
    advert << weekdays << (playing_at + 4).to_s << store_listing_file
    # p advert
    list_of_adverts << advert # unless ex_hal_store == ' EX HAL'

    # reset the timeslot counter
    playing_at = 9 if playing_at == 13
  end
end

list_of_adverts.each do |advert|
  line = advert.join(',')
  File.open(OUTPUT_FILE, 'a') do |file|
    file.write(line + "\n")
  end
end
